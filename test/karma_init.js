
import Vue from "vue";
import { before } from "mocha";

import {
  default as BaseVue,
  BaseLogger as logger } from "@cern/base-vue";

import { default as NtofVueWidgets } from "../src";

import d from "debug";
const debug = d("test:error");

before(function() {
  Vue.use(BaseVue);
  Vue.use(NtofVueWidgets);

  logger.autoReload = false; // do not reload browser on network error
  logger.on("error", (error) => debug("error:", error));
});
