//@flow
const { DisXmlState } = require("@ntof/dim-xml");

const State = {
  INITIALIZING: 0,
  WAITING: 1,
  PAUSED: 2,
  STOPPED: 3,
  RESETTING: 4
};

class TestState extends DisXmlState {
  /*::
  stateChanges: number
  static State: typeof State
  */
  constructor() {
    super();
    this.stateChanges = 0;
    this.addState(State.INITIALIZING, "Initialization");
    this.addState(State.WAITING, "Waiting");
    this.addState(State.PAUSED, "Paused");
    this.addState(State.STOPPED, "Stopped");
    this.addState(State.RESETTING, "Resetting");
    this.setState(State.INITIALIZING);
  }

  setState(value /*: number */) {
    ++this.stateChanges;
    return super.setState(value);
  }
}

TestState.State = State;

module.exports = TestState;
