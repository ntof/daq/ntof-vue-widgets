// @ts-check

import { createLocalVue as tuCreateLocalVue } from "@vue/test-utils";

import { defaultTo, get, has, toString } from "lodash";
import { makeDeferred } from "@cern/prom";
import { expect } from "chai";

import BaseVue from "@cern/base-vue";
import NtofVueWidgets from "../src";

/**
 * @typedef {import('@vue/test-utils').Wrapper<any>} Wrapper
 */
/**
 * @param  {() => boolean|any} cb
 * @param {string} [msg]
 * @param  {number} [timeout=1000]
 * @return {Promise<any>}
 */
export function waitFor(cb, msg = undefined, timeout = 1000) {
  var def = makeDeferred();
  var resolved = false;
  var err = new Error(msg || "timeout"); /* create this early to have stacktrace */
  /** @type {NodeJS.Timeout|null} */
  var nextTimer = null;
  var timer = setTimeout(() => {
    resolved = true;
    // @ts-ignore
    clearTimeout(nextTimer);
    def.reject(err);
  }, defaultTo(timeout, 1000));


  function next() {
    if (resolved) { return; }
    try {
      var ret = cb(); /* eslint-disable-line callback-return */
      if (ret) {
        clearTimeout(timer);
        resolved = true;
        def.resolve(ret);
      }
      else {
        nextTimer = /** @type {any} */(setTimeout(next, 200));
      }
    }
    catch (e) {
      clearTimeout(timer);
      resolved = true;
      // @ts-ignore
      def.reject(has(e, "message") ? e : new Error(e));
    }
  }

  next();
  return def.promise;
}

/**
 * @template T=Wrapper
 * @param  {() => T} test
 * @param  {string} [msg]
 * @param  {number} [timeout]
 * @return {Promise<T>}
 */
export function waitForWrapper(test, msg = undefined, timeout = undefined) {
  return waitFor(() => {
    const wrapper = test();
    // @ts-ignore
    return wrapper.exists() ? wrapper : false;
  }, msg, timeout);
}

/**
 * @param  {Function} fun
 */
function throws(fun) {
  try {
    fun();
    return null;
  }
  catch (/** @type {any} */ e) {
    return e.message;
  }
}

/**
 * @template T=any
 * @param  {() => T} test
 * @param  {any} value
 * @param  {string} [msg]
 * @param  {number} [timeout]
 * @return {Promise<T>}
 */
export function waitForValue(test, value,
                             msg = undefined, timeout = undefined) {
  /** @type {any} */
  var _val;
  return waitFor(() => {
    _val = test();
    return throws(() => expect(_val).to.deep.equal(value)) === null;
  }, msg, timeout)
  .catch((err) => {
    const assertMsg = throws(() => expect(_val).to.deep.equal(value)) ||
      ("Invalid result value: " + toString(_val) + " != " + value);
    if (err instanceof Error) {
      err.message = msg ? `${assertMsg}: (${msg})` : assertMsg;
      throw err;
    }
    throw new Error(msg);
  });
}

/**
 * @param {number} ms
 * @return {Promise<void>}
 */
export function wait(ms) {
  return new Promise((resolve) => {
    setTimeout(resolve, ms);
  });
}

/**
 * @param  {HTMLElement|Vue.Component|null}  el
 * @return {boolean}
 */
// eslint-disable-next-line complexity
export function isElementVisible(el) {
  if (!el) { return false; }
  /** @type {HTMLElement|null} */
  var elt;
  if (has(el, "element")) {
    elt = get(el, "element"); /* vue wrapper */
  }
  else if (has(el, "$el")) {
    elt = get(el, "$el"); /* vue component */
  }
  else {
    // @ts-ignore
    elt = el;
  }
  if (!(elt instanceof Element)) { return false; }
  while (elt instanceof Element) {
    const style = window.getComputedStyle(elt);
    if (elt.hidden ||
      (style &&
       (style.visibility === "hidden" ||
        style.display === "none"))) {
      return false;
    }
    elt = get(elt, "parentElement");
  }
  return true;
}

export const TransitionStub = {
  template: "<div :is=\"tag\"><slot></slot></div>",
  props: { tag: { type: String, default: "div" } }
};

export const stubs = {
  "transition-group": TransitionStub,
  transition: TransitionStub
};


export function createLocalVue() {
  const local = tuCreateLocalVue();
  local.use(BaseVue);
  local.use(NtofVueWidgets);
  return local;
}
