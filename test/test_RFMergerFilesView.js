// @ts-check
import { afterEach, beforeEach, describe, it } from "mocha";
import { mount } from "@vue/test-utils";
import { waitFor, waitForWrapper } from "./utils";
import { expect } from "chai";
import server from "@cern/karma-server-side";

import * as utilities from "../src/utilities";
import { RFMergerFilesView } from "../src";

describe("RFMergerFilesView", function() {
  /** @type {Tests.Wrapper} */
  let wrapper;
  /** @type {any} */
  let env;

  beforeEach(function() {
    return server.run(function() {
      const utils = serverRequire("./test/server_utils");
      const { NTOFStub } = serverRequire("@ntof/ntof-stubs");

      this.env = /** @type {any} */({});
      return utils.createApp(this.env)
      .then(() => {
        this.env.stub = new NTOFStub();
        return this.env.stub.init();
      })
      .then(() => {
        var addr = this.env.server.address();
        return {
          dns: this.env.stub.dns.url().substr(6),
          proxyUrl: `http://localhost:${addr.port}`
        };
      });
    })
    .then((ret) => {
      utilities.setCurrentUrl(ret.proxyUrl);
      env = ret;
    });
  });

  afterEach(function() {
    return server.run(function() {
      // this.env.eacs.close();
      this.env.server.close();
      this.env.stub.close();
      this.env = null;
    });
  });

  afterEach(function() {
    if (wrapper) {
      wrapper.destroy();
      // @ts-ignore
      wrapper = null;
    }
  });

  it("not initialized state", function() {
    wrapper = mount(RFMergerFilesView, {
      propsData: {
        dns: null,
        inEdit: false
      }
    });
    expect(wrapper.vm.client).to.be.undefined();
  });

  it("can list and sort current run files", async function() {
    /* dumb test, just validate that workers are working fine */
    wrapper = mount(RFMergerFilesView, {
      propsData: {
        proxy: env.proxyUrl,
        dns: env.dns
      }
    });

    expect(wrapper.vm.client).to.not.be.undefined();
    await waitFor(() => !wrapper.vm.fileLoading, "timeout fileLoading", 2000);
    expect(wrapper.findAll("tbody tr").length).to.equal(10);
    // ensure table is rendered for small screens
    wrapper.setData({ isScreenLG: false });
    // Change table PageSize to 25 items
    wrapper.find("select").setValue(25);
    await waitFor(() => !wrapper.vm.fileLoading, "timeout fileLoading", 2000);
    await waitFor(() => wrapper.findAll("tbody tr").length === 25, "now 25 items", 2000);
    let rows = wrapper.findAll("tbody tr");
    expect(wrapper.findAll("tbody tr").length).to.equal(25);
    // First result of the table is index 0
    // Check first row, second column (first is the collapsible icon)
    expect(rows.at(0).find(".x-rf-index").text()).to.equal("0");
    // Change ordering to Name (index) DESC (we are in small Screen mode!)
    const thHeads = wrapper.findAll("thead th");
    thHeads.filter((th) => th.html().includes("Index")).at(0).trigger("click");

    await waitFor(() => !wrapper.vm.fileLoading, "timeout fileLoading", 2000);
    rows = wrapper.findAll("tbody tr");
    expect(rows.length).to.equal(25);
    // First result of the table is now index 149
    // Check first row, second column (first is the collapsible icon)
    expect(rows.at(0).find(".x-rf-index").text()).to.equal("149");
  });

  it("can list files from RPC and change page", async function() {
    /* dumb test, just validate that workers are working fine */
    wrapper = mount(RFMergerFilesView, {
      propsData: {
        proxy: env.proxyUrl,
        dns: env.dns,
        runNumber: 900002
      }
    });

    await waitFor(() => !wrapper.vm.fileLoading, "timeout fileLoading", 2000);
    // ensure table is rendered for small screens
    wrapper.setData({ isScreenLG: false });

    await waitForWrapper(() => wrapper.find(".x-rf-index"));
    let rows = wrapper.findAll("tbody tr");
    expect(rows.length).to.equal(10);
    // First result of the table is index 0
    // Check first row, second column (first is the collapsible icon)
    expect(rows.at(0).find(".x-rf-index").text()).to.equal("0");

    // click on page 2
    const pageBtns = wrapper.findAll(".page-link");
    pageBtns.filter((btn) => btn.text() === "2")
    .at(0).trigger("click");

    await waitFor(() => !wrapper.vm.fileLoading, "timeout fileLoading", 2000);
    rows = wrapper.findAll("tbody tr");
    expect(rows.length).to.equal(10);
    // First result of the table is now index 10
    // Check first row, second column (first is the collapsible icon)
    expect(rows.at(0).find(".x-rf-index").text()).to.equal("10");
  });

  it("can ignore files", async function() {
    /* dumb test, just validate that workers are working fine */
    wrapper = mount(RFMergerFilesView, {
      propsData: {
        proxy: env.proxyUrl,
        dns: env.dns
      }
    });

    await waitFor(() => !wrapper.vm.fileLoading, "timeout fileLoading", 2000);
    expect(wrapper.findAll("tbody tr").length).to.equal(10);
    // Enable big Screen mode (easy to reach ignore switch)
    wrapper.setData({ isScreenLG: true });
    // Wait until the first TH of header has "File Name" instead of "Index"
    await waitFor(() => {
      const hRows = wrapper.findAll("thead th");
      return hRows.at(0).findAll("th").at(0).html().includes("File Name");
    }, "timeout TH of header", 2000);
    // Edit mode is disable, check that switch of the first row is disabled
    let rows = wrapper.findAll("tbody tr");
    let cSwitch = rows.at(0).find(".b-toggle-simple");
    /* $FlowIgnore: I do not know another way...*/
    expect(cSwitch.find("input").element.checked).to.be.false();
    expect(cSwitch.find("input").element.disabled).to.be.true();
    // Enable edit mode
    wrapper.setProps({ inEdit: true });
    // Wait until the first Ignore switch become editable
    await waitFor(() => {
      const rows = wrapper.findAll("tbody tr");
      const cSwitch = rows.at(0).find(".b-toggle-simple");
      return cSwitch.find("input").element.disabled !== true;
    }, "timeout switch editable", 2000);
    // Edit mode is enabled, check that switch of the first row is not disabled
    // and click it!
    rows = wrapper.findAll("tbody tr");
    cSwitch = rows.at(0).find(".b-toggle-simple");
    /* $FlowIgnore: I do not know another way...*/
    expect(cSwitch.find("input").element.checked).to.be.false();
    cSwitch.find("input").trigger("click");

    await waitFor(() => !wrapper.vm.fileLoading, "timeout fileLoading", 2000);
    // Check if the switch emitted "userEdit" event and it's checked (through props!)
    cSwitch = wrapper.findAll("tbody tr").at(0).find(".b-toggle-simple");
    expect(JSON.stringify(cSwitch.emitted())).to.include("userEdit");
    /* $FlowIgnore: I do not know another way...*/
    expect(cSwitch.find("input").element.checked).to.be.true();
  });

});
