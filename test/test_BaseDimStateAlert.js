// @ts-check
import { afterEach, beforeEach, describe, it } from "mocha";
import { mount } from "@vue/test-utils";
import { waitFor, waitForWrapper } from "./utils";
import { expect } from "chai";
import server from "@cern/karma-server-side";

import * as utilities from "../src/utilities";
import { BaseDimStateAlert as StateAlert } from "../src";
import _ from "lodash";

describe("StateAlert", function() {
  /** @type {Tests.Wrapper} */
  let wrapper;
  /** @type {any} */
  let env;

  beforeEach(function() {
    return server.run(function() {
      const utils = serverRequire("./test/server_utils");
      const { NTOFStub } = serverRequire("@ntof/ntof-stubs");
      const TestStateStub = serverRequire("./test/stub_State");

      this.env = /** @type {any} */({});
      return utils.createApp(this.env)
      .then(() => {
        this.env.stub = new NTOFStub();
        return this.env.stub.init();
      })
      .then(() => {
        this.env.stubState = new TestStateStub();
        return this.env.stub.node.addXmlState("myService/State", this.env.stubState);
      })
      .then(() => {
        var addr = this.env.server.address();
        return {
          dns: this.env.stub.dns.url().substr(6),
          proxyUrl: `http://localhost:${addr.port}`
        };
      });
    })
    .then((ret) => {
      utilities.setCurrentUrl(ret.proxyUrl);
      env = ret;
    });
  });

  afterEach(function() {
    return server.run(function() {
      // this.env.eacs.close();
      this.env.server.close();
      this.env.stub.close();
      this.env = null;
    });
  });

  afterEach(function() {
    if (wrapper) {
      wrapper.destroy();
      // @ts-ignore
      wrapper = null;
    }
  });

  it("changes alert in case of error", function() {
    wrapper = mount(StateAlert, {
      propsData: {
        proxy: env.proxyUrl, dns: env.dns, service: "myService/State",
        defaultString: "Please make a selection..."
      }
    });

    return waitForWrapper(() => wrapper.find(".alert-primary"))
    .then(() => {
      expect(wrapper.vm.defaultString).to.equal("Please make a selection...");
    })
    .then(() => {
      expect(wrapper.vm.errors).to.be.empty();
    })
    .then(() => server.run(function() {
      this.env.stubState.addError(100, "Test Warning");
    }))
    .then(() => waitFor(() => wrapper.find(".alert").classes("alert-danger")));
  });

  it("changes alert in case of warning", function() {
    wrapper = mount(StateAlert, {
      propsData: {
        proxy: env.proxyUrl, dns: env.dns, service: "myService/State",
        defaultString: "Please make a selection..."
      }
    });

    return waitForWrapper(() => wrapper.find(".alert-primary"))
    .then(() => {
      expect(wrapper.vm.defaultString).to.equal("Please make a selection...");
    })
    .then(() => {
      expect(wrapper.vm.warnings).to.be.empty();
    })
    .then(() => server.run(function() {
      this.env.stubState.addWarning(100, "Test Error");
    }))
    .then(() => waitFor(() => wrapper.find(".alert").classes("alert-warning")));
  });

  it("displays state information", function() {
    wrapper = mount(StateAlert, {
      propsData: { proxy: env.proxyUrl, dns: env.dns, service: "myService/State",
        defaultString: "Please make a selection..."
      }
    });

    return waitFor(() => !_.isNil(wrapper.vm.state))
    .then(() => {
      expect(wrapper.vm.state).to.equal(0);
    })

    // update the stub values
    .then(() => server.run(function() {
      this.env.stubState.setState(1);
    }))

    // ensure updates are being displayed on front-end
    .then(() => waitFor(() => (wrapper.vm.state === 1)));
  });
});
