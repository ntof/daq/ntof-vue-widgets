
import "./public-path";

import Vue from "vue";
import { default as BaseVue, createStore } from "@cern/base-vue";
import { default as NtofVueWidgets } from "../src";
import App from "./App";

Vue.use(BaseVue);
Vue.use(NtofVueWidgets);

export default new Vue({
  el: "#app",
  store: createStore(),
  components: { App },
  template: "<App/>"
});
