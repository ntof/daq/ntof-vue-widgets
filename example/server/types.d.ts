
export = AppServer
export as namespace AppServer

declare namespace AppServer {
    interface Config {
        port: number,
        basePath: string,
        noWebSocket?: boolean,
    }
}
