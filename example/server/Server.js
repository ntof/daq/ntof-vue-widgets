// @ts-check
const
  path = require("path"),
  express = require("express"),
  ews = require("express-ws"),
  pug = require("pug"),
  _ = require("lodash"),
  sse = require("./express-sse"),
  bodyParser = require("body-parser"),
  { makeDeferred } = require("@cern/prom"),
  { DisProxy, DnsProxy } = require("@ntof/redim-dim");

/**
 * @typedef {import('express').Request} Request
 * @typedef {import('express').Response} Response
 * @typedef {import('express').NextFunction} NextFunction
 */

class Server {
  /**
   * @param {AppServer.Config} config
   */
  constructor(config) {
    this.config = config;
    this.app = express();
    this._prom = this.prepare(config);
  }

  /**
   * @param {AppServer.Config} config
   */
  async prepare(config) {
    if (!_.get(config, "noWebSocket", false)) {
      ews(this.app);
    }

    this.app.set("view engine", "pug");
    this.app.set("views", __dirname);

    this.router = express.Router();
    this.app.use(bodyParser.text({ type: "text/plain" }));
    const dist = path.join(__dirname, "..", "..", "dist");
    this.router.use("/dist", express.static(dist));

    this.router.get("/", (req, res) => res.render("index", config));

    this.router.post("/pugRender", (req, res) => {
      res.setHeader("content-type", "text/plain");
      const ret = pug.render(req.body, { pretty: (req.query.pretty === "1") });
      res.send(ret);
    });

    this.router.get("/error_url", ({ next }) => {
      next({ status: 500, message: "Something went really wrong !" });
    });

    this.router.get("/sse", sse, (req, res) => {
      if (res.sse) {
        var counter = 0;
        const interval = setInterval(
          () => res.sse((++counter).toString()), 1000);
        res.on("close", () => clearInterval(interval));
      }
      else {
        res.send();
      }
    });

    DnsProxy.register(this.router);
    DisProxy.register(this.router);

    this.app.use(config.basePath, this.router);

    // default route
    this.app.use(function(req, res, next) {
      next({ status: 404, message: "Not Found" });
    });

    // error handler
    this.app.use(function(
      /** @type {any} */ err,
      /** @type {Request} */ req,
      /** @type {Response} */res) { // eslint-disable-line
      res.locals.message = err.message;
      res.locals.error = req.app.get("env") === "development" ? err : {};
      res.locals.status = err.status || 500;
      res.status(err.status || 500);
      res.render("error", { baseUrl: config.basePath });
    });
  }

  close() {
    if (this.server) {
      this.server.close();
      this.server = null;
    }
  }

  /**
   * @param {() => any} cb
   */
  async listen(cb) {
    await this._prom;
    const def = makeDeferred();
    /* we're called as a main, let's listen */
    var server = this.app.listen(this.config.port, () => {
      this.server = server;
      def.resolve(undefined);
      return _.attempt(cb || _.noop);
    });
    return def.promise;
  }

  address() {
    if (this.server) {
      return this.server.address();
    }
    return null;
  }

}

module.exports = Server;
