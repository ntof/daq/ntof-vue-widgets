// @ts-check

const
  { get, isNil, isArray, isString, split } = require("lodash"),
  debug = require("debug")("sse");

/**
 * @typedef {import('express').Request} Request
 * @typedef {import('express').Response} Response
 * @typedef {import('express').NextFunction} NextFunction
 */

/**
 * @this {Response}
 * @param  {string|Array<string>} msg
 * @param  {{ id?: any, type?: string }} [options]
 */
function sendEvent(msg, options) {
  const id = get(options, [ "id" ]);
  const type = get(options, [ "type" ]);

  debug("sse event");
  if (!isNil(id)) {
    this.write(`id: ${id}\n`);
  }
  if (!isNil(type)) {
    this.write(`event: ${type}\n`);
  }
  if (isString(msg) || !isArray(msg)) {
    msg = split(msg, "\n");
  }
  msg.forEach((m) => this.write(`data: ${m}\n`));
  this.write("\n");
  return this;
}

/**
 * @param  {Request} req
 * @param  {Response} res
 * @param  {NextFunction} next
 */
function sse(req, res, next) {
  if (get(req, [ "headers", "accept" ]) === "text/event-stream") {
    debug("sse request");
    res.sse = sendEvent;

    req.socket.setTimeout(0);
    req.socket.setNoDelay(true);
    req.socket.setKeepAlive(true);
    res.statusCode = 200;
    res.setHeader("Content-Type", "text/event-stream");
    res.setHeader("Cache-Control", "no-cache");
    res.setHeader("X-Accel-Buffering", "no");
    if (req.httpVersion !== "2.0") {
      res.setHeader("Connection", "keep-alive");
    }
  }
  next();
}

module.exports = sse;
