import { NextFunction, Request, Response } from "express";

declare module "express" {
  interface Response {
    sse?: (
      msg: string|string[],
      options?: { id?: any, type?: string }) => Response
  }
}

declare global {
  namespace Express {
    export interface Response {
        sse?: (
          msg: string|string[],
          options?: { id?: any, type?: string }) => this
    }
  }
}

declare function sse(req: Request, res: Response, next: NextFunction): void

export = sse
