// @ts-check
import Vue from "vue";

/**
 * @param {string} name
 * @param {any} defaultValue
 * @returns {function(any): any}
 */
export function getUiState(name, defaultValue = null) {
  return /** @this {Vue} */ function(state) {
    if (state[name] === undefined) {
      Vue.set(state, name, defaultValue);
      this.$store.commit("ui/update", { [name]: defaultValue });
    }
    return state[name];
  };
}
