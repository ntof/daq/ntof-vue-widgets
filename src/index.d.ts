import Vue from "vue";
import { ExtendedVue } from "vue/types/vue";
import { PluginFunction } from "vue/types/plugin";

export = NtofVueWidgets;
export as namespace NtofVueWidgets;

declare namespace NtofVueWidgets {

  export interface NtofVueWidgetsOptions {
  }

  const NtofVueWidgets: PluginFunction<NtofVueWidgetsOptions>;
  export default NtofVueWidgets;

  export function install(vue: Vue): void;

  export type ErrWarn = {
      code: number;
      message: string;
  };

  export const BaseDimStateAlert: ExtendedVue<Vue, {
      strState: string | null;
      state: number | null;
      errors: Array<ErrWarn> | null;
      warnings: Array<ErrWarn> | null;
      showDetails: boolean;
  }, {
      toggleDetails(): void;
  }, {
      placeholder: string;
      alertClass: string[];
      showError: boolean;
  }, {
      proxy: string;
      dns: string;
      service: string;
      primaryStyle: string;
      defaultString: string;
  }>;

  export const RFMergerFilesView: ExtendedVue<Vue, {
      fileLoading: boolean,
      isSendingCommand: boolean,
      isScreenLG: boolean,
      pageSize: number,
      currentPage: number,
      currentSort: string,
      currentSortDir: string,
      filesCount: number
  }, unknown, {
      dns: string;
      totalSize: number;
      totalTransferred: number;
      totalRate: number;
      fromRpc: boolean;
  }, {
      runNumber: number;
      inEdit: boolean;
  }>;
}
