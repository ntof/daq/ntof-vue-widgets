// @ts-check

import { has, isString, lowerFirst } from "lodash";
import Vue from "vue";

/** @type {string} */
var _currentUrl = window.location.origin + window.location.pathname;

/**
 * @return {string}
 */
export function currentUrl() {
  return _currentUrl;
}

/**
 * @param {string} url
 */
export function setCurrentUrl(url) {
  _currentUrl = url;
}

/**
 * @brief get dns full url. If proxy is not provided, current url is used.
 * @param  {string} dns DIM dns hostname
 * @param  {string?=} proxy DIM proxy hostname
 * @return {string} dns proxy url
 */
export function dnsUrl(dns, proxy) {
  return `${proxy ? proxy : _currentUrl}/${dns}/dns/query`;
}

export const UrlUtilitiesMixin = Vue.extend({
  methods: {
    /**
     * @param  {string?=} proxy
     */
    getProxy(proxy) {
      return proxy ? proxy : _currentUrl;
    },
    currentUrl() {
      return _currentUrl;
    },
    /**
     * @param  {string} dns
     * @param  {string?=} proxy
     */
    dnsUrl(dns, proxy) {
      return dnsUrl(dns, proxy);
    }
  }
});

/**
 * @param  {string} prefix string to append on errors
 * @param  {string|Error} err
 * @return {string|Error}
 */
export function errorPrefix(prefix /*: string*/, err /*: any */) {
  if (isString(err)) {
    return prefix + err;
  }
  else if (has(err, "message")) {
    err.message = prefix + lowerFirst(err.message);
  }
  throw err;
}


