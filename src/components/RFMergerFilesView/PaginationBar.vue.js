// @ts-check

import { max, min, range } from "lodash";
import Vue from "vue";

/**
 * @typedef {V.Instance<typeof component>} Instance
 */

const component = Vue.extend({
  name: "PaginationBar",
  props: {
    currentPage: { type: Number, default: 0 },
    totalRows: { type: Number, default: 0 },
    perPage: { type: Number, default: 0 }
  },
  computed: {
    /**
     * @this {Instance}
     * @return {number}
     */
    pagesCount() {
      return Math.ceil(this.totalRows / this.perPage);
    },
    /**
     * @this {Instance}
     * @return {Array<number>}
     */
    pagesToShow() {
      if (this.pagesCount <= 5) { return range(1, this.pagesCount + 1); }
      if (this.currentPage <= 2) { return range(1, 6); }
      if (this.currentPage >= this.pagesCount - 2) {
        return range(this.pagesCount - 4, this.pagesCount + 1);
      }
      const start = max([ 1, this.currentPage - 2 ]) || 1;
      const end = min([ this.pagesCount + 1, this.currentPage + 3 ]);
      return range(start, end);
    }
  },
  methods: {
    /** @param  {number} page */
    goToPage(page /*: number*/) {
      this.$emit("change-page", page);
    }
  }
});
export default component;
