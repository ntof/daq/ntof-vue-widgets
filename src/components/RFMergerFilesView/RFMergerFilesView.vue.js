// @ts-check
import Vue from "vue";
import { get, isEmpty } from "lodash";
import { UrlUtilitiesMixin } from "../../utilities";
import RFMergerCmdMixin from "../../mixins/RFMergerCmdMixin";
import {
  BaseMatchMedia as MatchMedia,
  BaseLogger as logger } from "@cern/base-vue";
import ProgressBar from "./ProgressBar.vue";
import PaginationBar from "./PaginationBar.vue";

import FileWorker from "../../workers/RFMergerFile.shared-worker.js";

/**
 * @typedef {import('../../interfaces/types').RFMRunFile} RFMRunFile
 * @typedef { V.Instance<typeof component> &
 *  V.Instance<typeof UrlUtilitiesMixin> &
 *  V.Instance<typeof RFMergerCmdMixin> } Instance
 */

const SortDir = {
  ASC: "asc",
  DESC: "desc"
};

const component = /** @type {V.Constructor<any, any>} */ (Vue).extend({
  name: "RFMergerFilesView",
  components: { ProgressBar, PaginationBar },
  mixins: [
    UrlUtilitiesMixin,
    RFMergerCmdMixin
  ],
  props: {
    proxy: { type: String, default: null },
    dns: { type: String, default: "" },
    runNumber: { type: Number, default: null },
    inEdit: { type: Boolean, default: false },
    showControls: { type: Boolean, default: true },
    showIgnoredField: { type: Boolean, default: true },
    initialSort: { type: String, default: "index" },
    initialSortDir: { type: String, default: SortDir.ASC },
    initialPageSize: { type: Number, default: 10 }
  },
  /**
   * @return {{
   *   client?: ?FileWorker,
   *   media?: ?MatchMedia,
   *   files: Array<RFMRunFile>,
   *   fileLoading: boolean,
   *   isSendingCommand: boolean,
   *   isScreenLG: boolean,
   *   pageSize: number,
   *   currentPage: number,
   *   currentSort: string,
   *   currentSortDir: string,
   *   filesCount: number,
   *   stats: ?{ size: number, transferred: number, rate: number },
   *   toggleRowStore: { [fileIndex: number]: boolean },
   *   rpcRefreshInterval?: NodeJS.Timeout }}
   */
  data() {
    return {
      files: [], fileLoading: true, isSendingCommand: false,
      isScreenLG: window.innerWidth > 992,
      pageSize: this.initialPageSize, currentPage: 1,
      currentSort: this.initialSort, currentSortDir: this.initialSortDir,
      filesCount: 0, stats: null,
      toggleRowStore: {}, rpcRefreshInterval: undefined
    };
  },
  computed: {
    /**
     * @this {Instance}
     * @return {number}
     */
    totalSize() { return get(this.stats, "size", 0); },
    /**
     * @this {Instance}
     * @return {number}
     */
    totalTransferred() { return get(this.stats, "transferred", 0); },
    /**
     * @this {Instance}
     * @return {number}
     */
    totalRate() { return get(this.stats, "rate", 0); },
    /**
     * @this {Instance}
     * @return {boolean}
     */
    fromRpc() { return !!this.runNumber; }
  },
  watch: {
    /** @this {Instance} */
    proxy() { this.loadData(); },
    /** @this {Instance} */
    dns() { this.loadData(); },
    /** @this {Instance} */
    runNumber() { this.loadData(); },
    /** @this {Instance} */
    pageSize() { this.loadData(); },
    /** @this {Instance} */
    stats() { this.$emit("update-stats", this.stats); }
  },
  /** @this {Instance} */
  beforeDestroy() {
    /** @type {MatchMedia} */ (this.media).close();
    if (this.rpcRefreshInterval) { clearTimeout(this.rpcRefreshInterval); }
    this.close();
  },
  /** @this {Instance} */
  mounted() {
    this.media = new MatchMedia(MatchMedia.LG,
      (/** @type {boolean} */ value) => { this.isScreenLG = value; });
    this.loadData();
  },
  methods: {
    /**
     * @this {Instance}
     * @param {number} index
     */
    toggleCollapseRow(index) {
      const toggle = get(this.toggleRowStore, index, false);
      this.$set(this.toggleRowStore, index, !toggle);
    },
    /**
     * @this {Instance}
     * @param {number} nPage
     */
    changePage(nPage) {
      this.currentPage = nPage;
      this.updateWorker();
    },
    /**
     * @this {Instance}
     * @param {string} field
     */
    setSorting(field) {
      if (this.currentSort === field) {
        this.currentSortDir = (this.currentSortDir === SortDir.ASC) ?
          SortDir.DESC : SortDir.ASC;
      }
      else {
        this.currentSort = field;
        this.currentSortDir = SortDir.ASC;
      }
      this.updateWorker();
    },
    /**
     * @this {Instance}
     * @param {string} field
     * @return {Array<string>}
     */
    getSortingClass(field) {
      const classes = [];
      if (this.currentSort !== field) {
        classes.push("fa-sort");
        classes.push("i-deactivated");
      }
      else if (this.currentSortDir === SortDir.ASC) {
        classes.push("fa-sort-down");
      }
      else {
        classes.push("fa-sort-up");
      }
      return classes;
    },
    /** @this {Instance} */
    close() {
      if (this.client) {
        this.client.port.postMessage({ destroy: true });
        this.client.port.close();
      }
      this.client = null;
    },
    /** @this {Instance} */
    updateWorker() {
      if (this.client) {
        this.client.port.postMessage({
          offset: (this.currentPage - 1) * this.pageSize,
          limit: this.pageSize,
          orderBy: this.currentSort +
            ((this.currentSortDir === SortDir.ASC) ? ":asc" : ":desc")
        });
        this.fileLoading = true;
      }
    },
    /** @this {Instance} */
    loadData() {
      if (isEmpty(this.dns)) { return; }

      this.close();
      this.client = new FileWorker();
      this.client.port.start();
      this.client.port.onmessage = this.onMessage;

      this.isSendingCommand = false;

      this.updateWorker();

      if (this.fromRpc) {
        this.requestRpc();
      }
      else {
        this.client.port.postMessage({
          service: "MERGER/Current/Files",
          options: {
            proxy: this.getProxy(this.proxy),
            dns: this.dnsUrl(this.dns, this.proxy)
          }
        });
      }
    },
    /** @this {Instance} */
    requestRpc() {
      this.client.port.postMessage({
        rpc: "MERGER/Files",
        args: { command: { $: { name: "listFiles", runNumber: this.runNumber } } },
        options: {
          proxy: this.getProxy(this.proxy),
          dns: this.dnsUrl(this.dns, this.proxy),
          timeout: 10000
        }
      });
    },
    /**
     * @this {Instance}
     * @param {any} msg
     */
    onMessage(msg) {
      const data = get(msg, "data");
      if (data.data) {
        this.files = data.data;
        this.filesCount = data.size;
        this.fileLoading = false;
        this.stats = data.stats;
      }
      else if (data.error) {
        logger.error(data.error);
      }
      // Automatic Refresh every 1 second if RPC mode
      if (this.fromRpc) {
        if (this.rpcRefreshInterval) { clearTimeout(this.rpcRefreshInterval); }
        this.rpcRefreshInterval = setTimeout(this.requestRpc.bind(this), 2000);
      }
    },
    /**
     * @this {Instance}
     * @param {RFMRunFile} file
     * @param {boolean} ignore
     */
    ignoreFileCmd(file, ignore) {
      if (this.isSendingCommand) { return; }
      this.isSendingCommand = true;
      this.rfmFileIgnore(file.runNumber, file.index, ignore, this.dns)
      .catch((/** @type {Error|string} */ err) => logger.error(err))
      .finally(() => { this.isSendingCommand = false; });
    },
    /**
     * @param  {RFMRunFile} file
     * @return {boolean}
     */
    canIgnoreBeModified(file) {
      if (!file.ignored) {
        return file.status !== "migrated";
      }
      return true;
    }
  }
});
export default component;
