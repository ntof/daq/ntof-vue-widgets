// @ts-check
import Vue from "vue";

/**
 * @typedef {V.Instance<typeof component>} Instance
 */

const component = Vue.extend({
  name: "ProgressBar",
  props: {
    progress: { type: Number, default: 0 },
    fileStatus: { type: String, default: "" }
  },
  computed: {
    /**
     * @this {Instance}
     * @return {Array<string>}
     */
    progressBarClass() {
      if (this.fileStatus === "ignored") {
        return [ "bg-secondary" ];
      }
      else if (this.progress === 100) {
        return [ "bg-success" ];
      }
      else if (this.progress > 0 && this.progress < 100) {
        return this.fileStatus === "transferring" ? [ "bg-primary" ] : [ "alert-warning" ];
      }
      return [ "bg-primary" ];
    }
  }
});
export default component;
