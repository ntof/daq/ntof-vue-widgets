// @ts-check

import Vue from "vue";
import { assign, clone, get, isEmpty } from "lodash";
import { DicXmlState } from "@ntof/redim-client";
import { UrlUtilitiesMixin } from "../utilities";
import {
  // @ts-ignore: no type for BaseAnimation, waiting for d3 migration
  BaseAnimation as Anim,
  BaseAnimationBlock as AnimBlock
} from "@cern/base-vue";

/**
 * @typedef {{ code: number, message: string }} ErrWarn
 * @typedef { V.Instance<typeof component> &
 *  V.Instance<typeof UrlUtilitiesMixin> } Instance
 */
const component = /** @type {V.Constructor<any, any>} */ (Vue).extend({
  name: "BaseDimStateAlert",
  components: { AnimBlock },
  mixins: [ UrlUtilitiesMixin ],
  props: {
    proxy: { type: String, default: "" },
    dns: { type: String, default: "" },
    service: { type: String, default: "" },
    primaryStyle: { type: String, default: "alert-primary" },
    defaultString: { type: String, default: "" }
  },
  /**
   * @returns {{
   *   strState: string | null,
   *   state: number | null,
   *   errors: Array<ErrWarn> | null,
   *   warnings: Array<ErrWarn> | null,
   *   showDetails: boolean,
   *   client: DicXmlState | null
   * }}
   */
  data() {
    return { strState: null, state: null,
      errors: null, warnings: null,
      showDetails: false, client: null };
  },
  computed: {
    /**
     * @this {Instance}
     * @returns {string}
     */
    placeholder() {
      return (isEmpty(this.dns) || isEmpty(this.service)) ?
        this.defaultString : "Waiting for service...";
    },
    /**
     * @this {Instance}
     * @returns {Array<string>}
     */
    alertClass() {
      if (!isEmpty(this.errors)) {
        return [ "alert-danger" ];
      }
      else if (!isEmpty(this.warnings)) {
        return [ "alert-warning" ];
      }
      else if (!isEmpty(this.dns) &&
               !isEmpty(this.service) &&
               isEmpty(this.strState)) {
        return [ "alert-warning" ];
      }
      return [ this.primaryStyle ];
    },
    /**
     * @this {Instance}
     * @returns {boolean}
     */
    showError() {
      return !isEmpty(this.errors) || !isEmpty(this.warnings);
    }
  },
  watch: {
    /** @this {Instance} */
    dns() { this.monitor(); },
    /** @this {Instance} */
    service() { this.monitor(); },
    /** @this {Instance} */
    state() {
      // @ts-ignore
      if (!this.$el.anim || !this.$el.anim.isPending()) {
        Anim.setOpacity(this.$el, 0.5);
        Anim.fade.in(this.$el, { duration: 0.5 });
      }
    }
  },
  /** @this {Instance} */
  mounted() {
    this.monitor();
  },
  /** @this {Instance} */
  beforeDestroy() {
    this.close();
  },
  methods: {
    /** @this {Instance} */
    close() {
      if (this.client) {
        this.client.removeListener("value", this.handleValue);
        this.client.close();
      }
      this.client = null;
    },
    /** @this {Instance} */
    toggleDetails() {
      // Only if there are errors or warning to show.
      if (this.showError) {
        this.showDetails = !this.showDetails;
      }
    },
    /**
     * @this {Instance}
     * @param {{
     *   strValue: string,
     *   value: number,
     *   errors: Array<ErrWarn>,
     *   warnings: Array<ErrWarn>,
     * }} value
     */
    handleValue(value/*: any */) {
      this.strState = get(value, "strValue");
      this.state = get(value, "value");
      this.errors = get(value, "errors");
      this.warnings = get(value, "warnings");
      this.$emit("state-change", clone(this.$data));

    },
    /** @this {Instance} */
    monitor() {
      this.close();
      // @ts-ignore: this is fine
      assign(this, this.$options.data());

      if (isEmpty(this.dns)) { return; }
      if (!isEmpty(this.service) && !isEmpty(this.dns)) {
        this.client = new DicXmlState(this.service,
          { proxy: this.getProxy(this.proxy) },
          this.dnsUrl(this.dns, this.proxy));
        this.client.on("value", this.handleValue);
        this.client.promise()
        .catch((err) => this.$emit("error", err));
      }
    }
  }
});
export default component;
