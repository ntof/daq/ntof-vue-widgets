
import { CombinedVueInstance, ExtendedVue, VueConstructor } from "vue/types/vue";
import { ComponentOptions, FunctionalComponentOptions,
  RecordPropsDefinition, ThisTypedComponentOptionsWithArrayProps, ThisTypedComponentOptionsWithRecordProps } from "vue/types/options";
import Vue from "vue";
import { Module as VuexModule, Store as VuexStore, StoreOptions as VuexStoreOptions } from "vuex";


export = V
export as namespace V

declare namespace V {
  type ComponentProps<T> = T extends ExtendedVue<
    any, any, any, any, infer Props>
    ? Props
    : any;

  type IfAny<T, Y, N> = 0 extends (1 & T) ? Y : N;

  type Instance<T, VueType extends Vue = Vue> = T extends ExtendedVue<
    VueType, infer Data, infer Methods, infer Computed, infer Props>
    ? (IfAny<Data, VueType, Data> & IfAny<Methods, VueType, Methods> &
        IfAny<Computed, VueType, Computed> & IfAny<Props, VueType, Props> & VueType) // eslint-disable-line @stylistic/js/max-len
    : VueType

  interface ExtVue<Opts, Refs> extends Vue {
    $options: ComponentOptions<Vue> & Opts;
    $refs: { [key: string]: Vue | Element | Vue[] | Element[] } & Refs;
  }

  // eslint-disable-next-line @stylistic/js/max-len
  interface Constructor<Opts, Refs, V extends Vue = Vue> extends VueConstructor<ExtVue<Opts, Refs>> {
  }

  interface Computed<Key, Type> { [Key]: Type }

  interface Shape<K> extends K {
    [any]: any
  }

  type StoreOptions<T> = VuexStoreOptions<T>;
  type Module<T, R=any> = VuexModule<T, R>;
  type Store<S> = VuexStore<S>;
}
