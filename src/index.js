// @ts-check

// Base
import * as components from "./components";
export * from "./components";

const NtofVueWidgets = {
  /**
   * @param {Vue.VueConstructor<Vue>} Vue
   */
  install(Vue) {
    // eslint-disable-next-line guard-for-in
    for (const prop in components) {
      /** @type {string|undefined} */ // @ts-ignore
      const name = components[prop].options.name;
      if (name) {
        // @ts-ignore
        Vue.component(name, components[prop]);
      }
      else {
        console.warn("[base-vue]: component name not set on: ", prop);
      }
    }
  }
};

export default NtofVueWidgets;
