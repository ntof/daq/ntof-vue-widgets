
export interface RFMRunFile {
    runNumber: number
    index: number
    name: string
    status: string
    ignored: boolean
    size: number
    transferred: number
    rate: number
    progress: number
}
