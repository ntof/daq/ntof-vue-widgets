// @ts-check

import { assign, bindAll, forEach, get, isArray, isNil, set, toNumber, transform, unset } from "lodash";

import { BaseCollectionWorker } from "@cern/base-web-workers";
import { DicXmlRpc, DicXmlValue, xml } from "@ntof/redim-client";
import { RFMRunFile, RunFilesMap, RunFilesMapRpc } from "../interfaces/rfmerger";
import { parseValue } from "../interfaces";

/**
 * @typedef {Partial<{
 *  service: string,
 *  options: any,
 *  rpc: string,
 *  args: any
 * }>} MessageIn
 * @typedef {{ size: number, transferred: number, rate: number }} Stats
 * @typedef {{ stats?: Stats } & BaseWorker.BaseCollectionWorker.MessageOut  } MessageOut
 */

/**
 * @param {{ [index: string]: any }} ret
 * @param {any} value
 * @param {string} key
 */
function mapIdx(ret, value, key) {
  ret[value.idx] = assign({ name: key }, value);
}

/** @type {{ [index: string]: { name: string, idx: number, type: number } }} */
const IdxParamMap = transform(RunFilesMap, mapIdx, {});
/** @type {{ [index: string]: { name: string, idx: number, type: number } }} */
const IdxParamMapRpc = transform(RunFilesMapRpc, mapIdx, {});

class RFMergerFileWorker extends BaseCollectionWorker {
  /**
   * @param {(msg: MessageOut) => any} post
   */
  constructor(post) {
    super(post);
    bindAll(this, [ "handleError", "handleValue" ]);
    /** @type {?DicXmlValue} */
    this.client = null;

    /** @type {MessageOut} */
    this.out = {};

    /** @type {?() => any} */
    this._abortFetch = null;
  }

  destroy() {
    this.close();
    super.destroy();
  }

  abortFetch() {
    if (this._abortFetch) {
      this._abortFetch();
      this._abortFetch = null;
    }
  }

  /**
   * @param  {BaseWorker.BaseCollectionWorker.MessageIn & MessageIn} msg
   * @return {void}
   */
  process(msg) {
    if (!msg) { return; }

    if (msg.service || msg.rpc) {
      // prevents filtering to automatically restart
      this.in.data = null;
      unset(this.out, "stats");
      this.abortFetch();
    }

    super.process(msg);

    if (msg.service) {
      this.close();
      this.in.data = null;

      this.client = new DicXmlValue(msg.service, msg.options,
        get(msg.options, "dns"));
      this.client.on("error", this.handleError);
      this.client.on("value", this.handleValue);
      this.client.promise().catch(this.handleError);
    }
    else if (msg.rpc) {
      this.close();
      var pending = true;
      this._abortFetch = () => {
        pending = false;
      };

      DicXmlRpc.invoke(msg.rpc, msg.args,
        msg.options, get(msg.options, "dns"))
      .then((value) => value.xml)
      .then((value) => {
        this._abortFetch = null; // too late to abort now
        if (pending) { this.handleValue(value, true); }
      })
      .catch(this.handleError);
    }
  }

  close() {
    if (this.client) {
      this.client.removeListener("value", this.handleValue);
      this.client.removeListener("error", this.handleError);
      this.client.close();
      this.client = null;
      this.abort();
    }
    unset(this.out, "stats");
  }

  /**
   * @param  {any} error
   */
  handleError(error) {
    this.post({ error: error });
  }

  /**
   * @param  {boolean} isIndex
   * @param  {number}  runNumber
   * @param  {boolean} isRpc
   * @param  {redim.XmlJsObject} data
   * @return {RFMRunFile}
   */
  createRunFile(isIndex, runNumber, isRpc, data) {
    const fileIndex = isIndex ? -1 : toNumber(get(data, [ "$", "index" ], -1));
    const fileObj = new RFMRunFile(runNumber, fileIndex);
    const idxParam = (isRpc ? IdxParamMapRpc : IdxParamMap);
    fileObj.name = get(data, [ "$", "name" ]);
    forEach(get(data, "data"), (data) => {
      const info = idxParam[get(data, [ "$", "index" ])];
      if (!info) {
        // indexes have an extra parameter
        if (get(data, [ "$", "name" ]) === "fileName") {
          fileObj.name = get(data, [ "$", "value" ]);
        }
        else {
          console.error("unknown data in file object", data);
          return;
        }
      }
      else {
        set(fileObj, info.name,
          parseValue(get(data, [ "$", "value" ], null), info.type));
      }
    });
    return fileObj;
  }

  /**
   * @param {Stats} stats
   * @param {RFMRunFile} fileObj
   */
  updateStats(stats, fileObj) {
    // calculate progress percentage
    if (!isNil(fileObj.size)) {
      stats.size += fileObj.size;
      if (fileObj.transferred > 0) {
        stats.transferred += fileObj.transferred;
        fileObj.progress = Math.round(fileObj.transferred * 100 / fileObj.size);
      }
      else if (fileObj.transferred === 0 && (fileObj.status === "migrated" || fileObj.status === "copied")) {
        fileObj.transferred = fileObj.size;
        stats.transferred += fileObj.transferred;
        fileObj.progress = 100;
      }
      else {
        fileObj.progress = 0; // assume not completed
      }
    }
    if (fileObj.rate > 0) {
      stats.rate += fileObj.rate;
    }
  }

  /**
   * @param {any} value
   * @param {boolean=} isRpc
   */
  handleValue(value, isRpc) {
    const dataJs = xml.toJs(value);
    // First data is the runNumber, second Nested dataset with files info
    if (!dataJs || !dataJs.data || dataJs.data.length < 2) {
      this.handleError("Something went wrong while retrieving files");
      return;
    }
    const runNumber = toNumber(get(dataJs, "data[0].$.value", -1));
    var stats = { size: 0, transferred: 0, rate: 0 };

    var data = get(dataJs, "data[1].data");
    if (data && !isArray(data)) {
      data = [ data ];
    }
    /** @type {RFMRunFile[]} */
    const ret = transform(data, (/** @type {RFMRunFile[]} */ ret, fileDataset) => {
      const fileObj = this.createRunFile(false, runNumber, isRpc || false,
        fileDataset);
      this.updateStats(stats, fileObj);
      ret.push(fileObj);
    }, []);

    if (dataJs.data.length >= 3) {
      const fileObj = this.createRunFile(true, runNumber, isRpc || false,
        get(dataJs, [ "data", 2 ]));
      this.updateStats(stats, fileObj);
      ret.push(fileObj);
    }
    this.out.stats = stats;
    super.process({ data: ret });
  }
}

BaseCollectionWorker.create = (p) => new RFMergerFileWorker(p);
