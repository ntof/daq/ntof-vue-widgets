import { expectType } from "tsd";
import { DicXmlState } from "@ntof/redim-client";
import { BaseDimStateAlert, ErrWarn } from ".";


(async function() {

  const state = new BaseDimStateAlert();
  expectType<string | null>(state.strState);
  expectType<number | null>(state.state);
  expectType<null | Array<ErrWarn>>(state.errors);
  expectType<null | Array<ErrWarn>>(state.warnings);
  expectType<boolean>(state.showDetails);
  expectType<DicXmlState | null>(state.client);
}());
